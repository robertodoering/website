// collapse navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top >= $(window).height() ||
        $('html, body').is(':animated')) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});

// blur links after click
$("a").mouseup(function() {
    $(this).blur();
});

// navbar page scroll
$(function() {
    $('a.page-scroll').click(function(event) {
        event.preventDefault();

        // stop animation when scrolling
        $('html, body').on('scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove', function () {
            $('html, body').stop();
        });

        $('html, body').stop().animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 600, 'easeOutQuart');
    });
});

// project show more scroll
$(function() {
    $.each($('.collapse'), function(i, obj) {
        $('#' + obj.id).on('shown.bs.collapse', function(event) {
            // stop animation when scrolling
            $('html, body').on('scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove', function () {
                $('html, body').stop();
            });

            $('html, body').stop().animate({
                scrollTop: $($('#' + obj.id)).offset().top - 50
            }, 600, 'easeOutQuart');
        });

        $('#' + obj.id).on('hide.bs.collapse', function(event) {
            // stop animation when scrolling
            $('html, body').on('scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove', function () {
                $('html, body').stop();
            });

            $('html, body').stop().animate({
                scrollTop: $($('#software')).offset().top
            }, 600, 'easeOutQuart');
        });
    });
});
